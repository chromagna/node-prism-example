import { createSchema, ensure } from "node-prism";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

// The TypeORM entity
@Entity()
export class Photo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length: 100,
    })
    name: string;

    @Column("text")
    description: string;

    @Column()
    filename: string;

    @Column("double")
    views: number;

    @Column()
    isPublished: boolean;
}

// A validator for inserting records
export const PhotoInsertValidator = createSchema({
    name: ensure.string().notEmpty().max(100),
    description: ensure.string().notEmpty().optional(),
    filename: ensure.string().notEmpty().max(100),
    views: ensure.number().optional(),
    isPublished: ensure.boolean().optional(),
});

// A validator for updating records
export const PhotoUpdateValidator = createSchema({
    id: ensure.number().integer(),
    name: ensure.string().notEmpty().optional(),
    description: ensure.string().notEmpty().optional(),
    filename: ensure.string().notEmpty().optional(),
    views: ensure.number().integer().min(0).optional(),
    isPublished: ensure.boolean().optional(),
});
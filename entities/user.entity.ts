// import { Validators } from "node-prism";
import { createSchema, ensure } from "node-prism";
import { BeforeInsert, Column, Entity, Generated, PrimaryColumn } from "typeorm";
import md5 from "../utils/md5";


@Entity()
export class User {
    @Column({ length: 50 })
    public username: string;

    @PrimaryColumn()
    public email: string;

    @Column()
    public password: string;

    @Column()
    @Generated("uuid")
    public guid: string;

    @BeforeInsert()
    public getSaltPassword() {
        this.password = md5.init(this.password);
    }
}

export const UserInsertValidator = createSchema({
    username: ensure.string().notEmpty().max(50),
    description: ensure.string().notEmpty().email(),
    password: ensure.string().notEmpty(),
});
import { ActionMiddleware, Context, Controller, DocAction, FromBody, GET, JWT, POST, Respond, Response } from "node-prism";
import App from "../app";
import { AuthenticationMiddleware } from "../middleware/auth.middleware";

@Controller("/api/auth")
export class AuthenticationController {
    @POST("")
    @DocAction("Logs you in.")
    login(context: Context, @FromBody("data") data: object): Response<string> {
        const payload = {
            hello: "world",
            iat: Date.now(),
            exp: Date.now() + 60 * 1000,
        };

        const token_str = JWT.Encode(payload, App.JWT_ENCODING_KEY);
        return Respond.OK(context, token_str);
    }

    @GET("/protected")
    @DocAction("An example route protected by JWT authentication.")
    @ActionMiddleware(AuthenticationMiddleware.ValidateToken)
    protect(context: Context): Response<string> {
        return Respond.OK(context, "You're allowed.");
    }
}
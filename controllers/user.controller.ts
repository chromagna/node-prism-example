import { Context, Controller, FromBody, GET, POST, Respond, Response } from "node-prism";
import { User } from "../entities/user.entity";
import { UserService } from "../services/user.service";


@Controller("/api/user")
export class UserController {
    @POST("")
    async createUser(context: Context, @FromBody("data") data): Promise<Response<User>> {
        return UserService.InsertUser(data)
            .then((user: User) => {
                return Respond.OK(context, user);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }

    @GET("")
    async getAllUsers(context: Context): Promise<Response<User[]>> {
        return UserService.GetAllUsers()
            .then((users: User[]) => {
                return Respond.OK(context, users);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }

    @POST("check")
    async checkPassword(context: Context, @FromBody("data") data: { email: string, password: string }): Promise<Response<boolean>> {
        return UserService.DoPasswordsMatch(data)
            .then((bool: boolean) => {
                return Respond.OK(context, bool);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }
}
import * as Express from "express";
import { ActionMiddleware, Context, Controller, ControllerMiddleware, DELETE, DocAction, DocController, FromBody, FromPath, GET, Middleware, PATCH, POST, Respond, Response } from "node-prism";
import { Photo } from "../entities/photo.entity";
import { PhotoService } from "../services/photo.service";


@Controller("/photo")
@DocController("This controller provides a REST interface to the Photo entity.")
@ControllerMiddleware(PhotoController.LoggingMiddleware)
export class PhotoController {
    static LoggingMiddleware(req: Express.Request, res: Express.Response, next: Function) {
        console.log("LoggingMiddleware:", req.method, "/api/photo" + req.path);
        next();
    }

    @POST("/upload")
    @DocAction("This endpoint handles photo uploads.")
    @ActionMiddleware(Middleware.Throttle(2)) // 2 requests per second per unique host.
    async upload(context: Context): Promise<Response<Photo>> {
        return PhotoService.UploadPhoto(context, {
            dir: "photos",
            scrambleFilename: true,
            formFieldName: "file",
            returnBuffer: false,
        })
            .then((photo: Photo) => {
                return Respond.OK(context, photo, "Photo uploaded.");
            })
            .catch((e: Error) => {
                return Respond.BadRequest(context, e);
            });
    }

    @GET("")
    @DocAction("Get all photos.")
    async getAllPhotos(context: Context): Promise<Response<Photo[]>> {
        return PhotoService.GetAllPhotos()
            .then((photos: Photo[]) => {
                return Respond.OK(context, photos);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }

    @GET("/:id")
    @DocAction("Get a photo by ID.")
    async getPhoto(context: Context, @FromPath("id", parseInt) id: number): Promise<Response<Photo>> {
        return PhotoService.GetPhotoById(id)
            .then((photo: Photo) => {
                return Respond.OK(context, photo);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }

    @POST("")
    @DocAction("Insert a test photo.")
    async postPhoto(context: Context, @FromBody("data") data): Promise<Response<Photo>> {
        return PhotoService.InsertPhoto(data)
            .then((photo: Photo) => {
                return Respond.OK(context, photo);
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }

    @DELETE("/:id")
    @DocAction("Remove a photo by ID.")
    async deletePhoto(context: Context, @FromPath("id", parseInt) id: number): Promise<Response<Photo>> {
        return PhotoService.DeletePhoto(id)
            .then((photo: Photo) => {
                return Respond.OK(context, photo, "Photo was removed.");
            })
            .catch((e: Error) => {
                return Respond.BadRequest(context, e);
            });
    }

    @PATCH("/:id")
    @DocAction("Update a photo by ID.")
    async updatePhoto(context: Context, @FromPath("id", parseInt) id: number, @FromBody("data") data: object): Promise<Response<Photo>> {
        return PhotoService.UpdatePhoto(id, data)
            .then((photo: Photo) => {
                return Respond.OK(context, photo, "Photo was updated.");
            })
            .catch((e: Error) => {
                return Respond.BadRequest(context, e);
            });
    }

    @GET("/mail/:id")
    @DocAction("Send a test email.")
    async sendTestMail(context: Context, @FromPath("id", parseInt) id: number): Promise<Response<any>> {
        return PhotoService.SendPhotoAttachment(id, "recipient@somehost.com")
            .then(() => {
                return Respond.OK(context, {}, `Photo with id ${id} was emailed to you.`)
            })
            .catch((e: Error) => {
                return Respond.InternalServerError(context, e);
            });
    }
}
import * as Express from "express";
import * as Http from "http";
// import * as P from "node-prism";
import { initialize, Communication } from "node-prism";
import { Connection, createConnection } from "typeorm";
import { Photo } from "./entities/photo.entity";
import { User } from "./entities/user.entity";

createConnection({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "prism",
    password: "prism",
    database: "node-prism",
    entities: [
        // __dirname + "/entities/*.js"
        Photo,
        User
    ],
    synchronize: true,
    logging: false,
}).then(connection => {
    App.connection = connection;
    App.init();
    App.start();
    App.commandServer();
}).catch(error => console.log(error));

export default class App {

    private static port: number = 4000;
    private static server: Http.Server;
    private static express: Express.Express;
    /**
     * This is static so that our services can get repository references with:
     * const PhotoRepository = App.connection.manager.getRepository(Photo);
     */
    public static connection: Connection;
    public static JWT_ENCODING_KEY: string = "supersecretpassword";

    public static init(): void {
        App.express = Express();
        App.express.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "*");
            res.header("Access-Control-Allow-Headers", "*");

            if (req.method === "OPTIONS") {
                return res.status(200).end();
            }

            return next();
        });

        App.express.use(Express.json());
        App.express.use(Express.urlencoded({ extended: false }));
    }

    public static start(): void {
        App.server = Http.createServer(App.express);
        initialize(App.express, "dist/controllers");
        App.server.listen(App.port);

        console.log(`Listening on http://localhost:${App.port}`);
    }

    static commandServer() {
        const cs = new Communication.CommandServer({
            host: "localhost",
            port: 8090,
            useInsecure: true, // or false, then..
            keepAlive: true,
            keepAliveInitialDelay: 60000,
        });

        cs.command(0, async payload => {
            payload.received_at = Date.now();
            return payload;
        });
    }
}
This project serves as a demonstration of https://gitlab.com/chromagna/node-prism.

# Requirements

This example project uses TORM to connect to a MySQL database on port 3306. See `app.ts` for connection details.
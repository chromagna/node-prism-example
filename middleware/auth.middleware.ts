import * as Express from "express";
import App from "../app";
import { Bearer, Context, JWT, Respond, Response } from "node-prism";

export class AuthenticationMiddleware {
    public static get ValidateToken(): Express.Handler {
        return function ValidateTokenMiddleware(req: Express.Request, res: Express.Response, next: Function): Express.Response<Response<any>> {
            let token = Bearer.GetToken(req);
            let c = new Context();

            c.request = req;
            c.response = res;
            c.nextMiddleware = next;

            if (token) {
                const verify_result: JWT.VerifyResult = JWT.Verify(token, App.JWT_ENCODING_KEY);
                if (verify_result.exp) {
                    return res.json(Respond.Unauthorized(c, "That token has expired."));
                }

                if (!verify_result.sig) {
                    return res.json(Respond.Unauthorized(c, "That token's signature could not be validated."));
                }

                return next();
            }

            return res.json(Respond.Unauthorized(c, "Bad or no token provided."));
        };
    }
}
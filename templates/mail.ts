export interface IPhotoMailerOptions {
    subject: string;
    photo_name: string;
};

export interface IRegistrationConfirmationOptions {
    subject: string;
    username: string;
};

export interface IMailTemplate {
    subject: string;
    body: string;
}

export const RegistrationConfirmation = (vars: IRegistrationConfirmationOptions): IMailTemplate => {
    return {
        subject: vars.subject,
        body: `Hello ${vars.username}! Thanks for ...`,
    };
};

export const PhotoMailer = (vars: IPhotoMailerOptions): IMailTemplate => {
    return {
        subject: vars.subject,
        body: `Here's ${vars.photo_name}`,
    };
};
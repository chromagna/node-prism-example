import { ReadStream } from "fs";
import * as nodemailer from "nodemailer";
import { IMailTemplate } from "../templates/mail";

// nodemailer's transport.sendMail returns more than this, but
// for this example, these properties are all we care about.
interface MailResponse {
    response: any;
    envelope: any;
}

export class MailService {
    private static getTransport(): nodemailer.Mailer {
        return nodemailer.createTransport({
            host: "smtp.yourmailhost.com",
            port: 587,
            secure: false,
            auth: {
                user: "support@yourmailhost.com",
                pass: "verysecurepassword",
            },
        });
    }

    public static async Send(template: IMailTemplate, to: string): Promise<MailResponse> {
        const transporter = MailService.getTransport();
        return await transporter.sendMail({
            from: '"Support" <support@yourmailhost.com>',
            to: `"${to}"`,
            subject: template.subject,
            html: template.body,
        });
    }

    public static async SendWithAttachment(template: IMailTemplate, to: string, stream: ReadStream, filename: string): Promise<MailResponse> {
        const transporter = MailService.getTransport();
        return await transporter.sendMail({
            attachments: [
                {
                    filename: filename,
                    content: stream,
                }
            ],
            from: '"Support" <support@yourmailhost.com>',
            to: `"${to}"`,
            subject: template.subject,
            html: template.body,
        });
    }
}
import App from "../app";
import { User } from "../entities/user.entity";

const UserRepository = App.connection.manager.getRepository(User);

export class UserService {
    public static async GetAllUsers(): Promise<User[]> {
        return await UserRepository.find({
            order: {
                email: "DESC"
            }
        });
    }

    public static async InsertUser(data): Promise<User> {
        let user = new User();
        user = Object.assign(user, data);

        // Validated, but is this email already registered?
        const count = await UserRepository.createQueryBuilder("user")
            .where("user.email = :email", { email: user.email })
            .getCount();

        if (count > 0) throw new Error("A user with that email already exists.");

        return await UserRepository.save(user);
    }

    public static async DoPasswordsMatch(data: { email: string, password: string }): Promise<boolean> {
        const count = await UserRepository.createQueryBuilder("user")
            .where("user.email = :email", { email: data.email })
            .andWhere("user.password = md5(:pwd)", { pwd: data.password })
            .getCount();

        return count > 0;
    }
}
import { createReadStream } from "fs";
import { Context, FileRequest, IUploadConfig, ProcessFileUploadRequest } from "node-prism";
import App from "../app";
import { Photo, PhotoInsertValidator, PhotoUpdateValidator } from "../entities/photo.entity";
import { IPhotoMailerOptions, PhotoMailer } from "../templates/mail";
import { MailService } from "./mail.service";


const PhotoRepository = App.connection.manager.getRepository(Photo);

export class PhotoService {
    public static async UploadPhoto(context: Context, opts: IUploadConfig): Promise<Photo> {
        try {
            let resp: FileRequest = await ProcessFileUploadRequest(context, opts);
            let photo = new Photo();

            photo.filename = resp.fileName;
            photo.name = resp.originalFileName;
            photo.views = 0;
            photo.isPublished = true;

            if (!PhotoInsertValidator(photo).ok) {
                throw new Error("Photo does not pass validation.");
            }

            return await PhotoRepository.save(photo);
        } catch (err) {
            throw new Error(err);
        }
    }

    public static async GetAllPhotos(): Promise<Photo[]> {
        return await PhotoRepository.find({
            order: {
                id: "DESC"
            }
        });
    }

    public static async GetPhotoById(id: number): Promise<Photo> {
        let p: Photo = await PhotoRepository.findOne(id);
        if (p) return p;
        else throw new Error(`A photo by that identifier (${id}) could not be retrieved.`);
    }

    public static async SendPhotoAttachment(id: number, address: string) {
        return PhotoService.GetPhotoById(id)
            .then(async (photo: Photo) => {
                let templateOptions: IPhotoMailerOptions = {
                    subject: `Your photo download request`,
                    photo_name: photo.name,
                };
                let template = PhotoMailer(templateOptions);
                let stream = createReadStream(`./photos/${photo.filename}`);
                await MailService.SendWithAttachment(template, address, stream, photo.name);
            })
            .catch((err) => {
                throw new Error(err);
            });

    }

    public static async InsertPhoto(data: object): Promise<Photo> {
        let photo = new Photo();
        photo = Object.assign(photo, data);
        photo.isPublished = true;

        if (!PhotoInsertValidator(photo).ok) {
            throw new Error("Data provided did not pass validation.");
        }

        return await PhotoRepository.save(photo);
    }

    public static async DeletePhoto(id: number): Promise<Photo> {
        let photo = await PhotoRepository.findOne(id);
        if (!photo) throw new Error(`Photo with id '${id}' could not be found.`);
        return PhotoRepository.remove(photo);
    }

    public static async UpdatePhoto(id: number, data: object): Promise<Photo> {
        let photo = await PhotoRepository.findOne(id);
        if (!photo) throw new Error(`Photo with id '${id}' could not be found.`);
        photo = Object.assign(photo, data);

        if (!PhotoUpdateValidator(photo).ok) {
            let s = JSON.stringify(PhotoUpdateValidator(photo).errors);
            throw new Error(s);
        }

        return await PhotoRepository.save(photo);
    }
}
